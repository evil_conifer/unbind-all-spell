##Unbind-All Spell
_____

An Extension of MagicSpells [http://nisovin.com]
Allows users to unbind all spells currently bound to their item in hand

###Details
_____

**Author**: EvilConifer
**Version**: 6.25.89a
**Language**: Java
**Dependencies**: - Bukkit  http://dl.bukkit.org - MagicSpells  http://nisovin.com
**Website**: http://pigcraft.net
**Type**: Addon/Plugin
**License**: See @LICENSE.txt

###Installation and Configuration

_____

**Step 1**: Download the binary .jar file from
**Step 2**: Drop the .jar file into *.../plugins/MagicSpells*
**Step 3**: Add the following to your *spells-command.yml* file:
    unbind-all:
        spell-class: "main.java.net.pigcraft.code.spells.unbindall.UnbindAllSpell"
        name: unbind-all
        description: Unbinds all current spells from an item