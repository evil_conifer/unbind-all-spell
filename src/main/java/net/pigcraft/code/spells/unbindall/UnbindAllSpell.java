package net.pigcraft.code.spells.unbindall;

import com.nisovin.magicspells.MagicSpells;
import com.nisovin.magicspells.Spellbook;
import com.nisovin.magicspells.spells.CommandSpell;
import com.nisovin.magicspells.util.MagicConfig;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * An Extension of MagicSpells [http://nisovin.com]
 * Allows users to unbind all spells currently bound to their item in hand
 *
 * /!\ THIS CODE IS LICENSED UNDER THE UNILICENSE <http://unlicense.org/>, SEE LICENSE.txt FOR MORE INFORMATION /!\
 *
 * @author EvilConifer
 * @version 2.6
 */
public class UnbindAllSpell extends CommandSpell {
    public UnbindAllSpell(MagicConfig config, String spellName) {
        super(config, spellName);
    }

    @Override
    public PostCastAction castSpell(Player player, SpellCastState state, float pow, String[] args) {

        if (state == SpellCastState.NORMAL) {

            Spellbook sb = MagicSpells.getSpellbook(player);
            if(sb == null){
                sendMessage(player,"You do not know a spell by that name!");
                return PostCastAction.ALREADY_HANDLED;
            }else{
                try{
                    sb.removeAllCustomBindings();
                    sendMessage(player, "&aSuccessfully removed all spells from selected item");
                }catch(Exception e){
                    sendMessage(player, "&4Error occurred, please see Console output for more information.");
                    MagicSpells.plugin.getLogger().severe("Error occurred while attempting to execute spell unbind-all");
                    e.printStackTrace();
                }

            }



        }

        return PostCastAction.HANDLE_NORMALLY;

    }





    @Override
    public boolean castFromConsole(CommandSender sender, String[] args) {
        sender.sendMessage("This command cannot be executed from the console!");
        return false;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String partial) {

        if (sender instanceof Player) {
            if (partial.contains(" ")) {
                return null;
            }

            return tabCompleteSpellName(sender, partial);
        }


        return null;
    }



}
